import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.css']
})
export class SendMessageComponent implements OnInit {
  newmessage:string;
  constructor(private messageService: MessageService, private toaster: ToastrService) { }

  ngOnInit() {
    this.newmessage = "";
  }

  sendMessage() {    
    const body = {'message': this.newmessage};
    this.messageService.sendMessage(body).subscribe((data:any) => {
      this.toaster.info('Message sent.'+' '+ data.sid);
    })
  }

}
