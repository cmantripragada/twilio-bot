const express = require('express')
const router = express.Router();

const accountSid = 'ACbf1ff025db3c2f0a067bbd45cdc997d4';
const authToken = '51fec3e7527acf2d80b473c7d5d383d7';
const client = require('twilio')(accountSid, authToken);

router.post('/', (req, res) => {
    client.messages.create({
        body: req.body.message,
        from: '+12562448150',
        to: '+918668740926'
    }).then(message => {
        res.json(message);
    }).catch(error => {
        res.json(error);
    });
});

module.exports = router;