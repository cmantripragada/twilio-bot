const http = require('http')
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const message = require('./router');
const MessagingResponse = require('twilio').twiml.MessagingResponse;

// Run the app by serving the static files
// in the dist directory
app.use(express.static(process.cwd() + '/dist/twilio-bot'));
app.get('/*', function (req, res) {
    res.sendFile(path.join(process.cwd() + '/dist/twilio-bot/index.html'));
});
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

/* send message to numbers */
app.use('/send', message);

/* webhook responding to incoming receive to twilio*/
app.get('/sms', (req, res) => {
    const twiml = new MessagingResponse();
    twiml.message('The Robots are coming! Head for the hills!');
    res.writeHead(200, { 'Content-Type': 'text/xml' });
    res.end(twiml.toString());
});

http.createServer(app).listen(8081, () => {
    console.log('Express server listening on port 8081');
});